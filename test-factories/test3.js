/**
 * Created by michaeltzach on 29/11/2017.
 */

const fs = require('fs');

let sites = [];
for (var i = 0; i < 5000; i++) {
    sites.push("www." + i + ".com");
}

let inputString = "";
for (var i = 0; i < 5000; i++) {
    inputString += sites[i] + " someTag\n";
}

let sortedSites = sites.sort();

let outputString = "";
for (var i = 0; i < 5000; i++) {
    let numberOfRelatedForSite = 0;
    for (var j = 0; j < 11; j++) {
        if (i === j) continue;

        if (numberOfRelatedForSite < 10) {
            outputString += sortedSites[i] + "\t" + sortedSites[j] + " 1\n";
            numberOfRelatedForSite++;
        }
    }
}

const testNumber = 3;
writeToFile(inputString, "test" + testNumber + "input");
writeToFile(outputString, "test" + testNumber + "output");

function writeToFile(data, fileName) {
    const path = process.cwd() + '/' + fileName;
    if (fs.existsSync(path)) {
        fs.unlinkSync(path);
    }
    fs.writeFileSync(path, data);
}
