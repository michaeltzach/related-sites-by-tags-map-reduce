/**
 * Created by michaeltzach on 29/11/2017.
 */

const fs = require('fs');
let letters = "a b c d e f g h i j k l m n o p q r s t u v w x y z";
let lettersArray = letters.split(" ");

let sites = [];

for (var i = 0; i < 10; i++) {
    sites.push("www." + lettersArray[i] + ".com");
}

let tags = [];
for (var i = 0; i < 10; i++) {
    tags.push(lettersArray[i]);
}

let inputString = "";
for (var i = 0; i < 10; i++) {
    for (var j = 0; j < 10; j++) {
        inputString += sites[i] + " " + tags[j] + "\n";
    }
}

let outputString = "";
for (var i = 0; i < 10; i++) {
    for (var j = 0; j < 10; j++) {
        if (i == j) continue;
        outputString += sites[i] + "\t" + tags[j] + " 10\n";
    }
}

const testNumber = 1;
writeToFile(inputString, "test" + testNumber + "input");
writeToFile(outputString, "test" + testNumber + "output");

function writeToFile(data, fileName) {
    const path = process.cwd() + '/' + fileName;
    if (fs.existsSync(path)) {
        fs.unlinkSync(path);
    }
    fs.writeFileSync(path, data);
}