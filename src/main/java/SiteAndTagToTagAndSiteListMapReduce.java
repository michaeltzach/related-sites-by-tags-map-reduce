import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;
import java.util.Iterator;

public class SiteAndTagToTagAndSiteListMapReduce {
    public static class WebsiteToTagMapper extends Mapper<Object, Text, Text, Text> {
        private Text websiteURL = new Text();
        private Text tag = new Text();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String[] websiteAndTagArray = value.toString().split("\\s+"); //split by whitespaces*

            if (websiteAndTagArray.length != 2) {
                return;
            }

            websiteURL.set(websiteAndTagArray[0]);
            tag.set(websiteAndTagArray[1]);

            context.write(tag, websiteURL);
        }
    }

    public static class WebsiteLinksReducer extends Reducer<Text, Text, Text, Text> {
        private final static String websiteOutputDelimiter = " ";
        private Text webSiteList = new Text();

        protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            StringBuilder websitesListStringBuilder = new StringBuilder();
            Iterator<Text> websitesForTagIterator = values.iterator();

            while (websitesForTagIterator.hasNext()) {
                websitesListStringBuilder.append(websitesForTagIterator.next().toString());
                if (websitesForTagIterator.hasNext()) {
                    websitesListStringBuilder.append(websiteOutputDelimiter);
                }
            }

            webSiteList.set(websitesListStringBuilder.toString());
            context.write(key, webSiteList);
        }
    }

    public static Job createJob(Path input, Path output, Configuration configuration) throws Exception {
        Job job = Job.getInstance(configuration, "Web site and tag to tag and site list");
        job.setJarByClass(TagMatcher.class);
        job.setMapperClass(WebsiteToTagMapper.class);
        job.setReducerClass(WebsiteLinksReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);
        return job;
    }
}
