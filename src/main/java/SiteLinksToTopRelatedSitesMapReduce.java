import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Iterator;

public class SiteLinksToTopRelatedSitesMapReduce {
    static String compositeKeyDelimiter = " ";

    public static class RelatedSiteAndLinkStrength implements Writable {

        String relatedSite;
        String linkStrength;

        @Override
        public void write(DataOutput dataOutput) throws IOException {
            dataOutput.writeBytes(relatedSite + " " + linkStrength);
        }

        @Override
        public void readFields(DataInput dataInput) throws IOException {
            String[] dataComponents = dataInput.readLine().split(" ");
            this.relatedSite = dataComponents[0];
            this.linkStrength = dataComponents[1];
        }
    }

    public static class WebsiteLinkStrengthToCompositeKeyMapper extends Mapper<Object, Text, Text, RelatedSiteAndLinkStrength> { //TODO: change output to be object
        private Text combinedKey = new Text();
        private RelatedSiteAndLinkStrength relatedSiteAndLinkStrength = new RelatedSiteAndLinkStrength();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            //"{first site} {second site} {strength}"
            String[] valueComponents = value.toString().split("\\s+"); //split by whitespaces*

            if (valueComponents.length != 3) {
                throw new InterruptedException("Bad input for website link strength mapper");
            }

            String keySite = valueComponents[0];
            String relatedSite = valueComponents[1];
            String linkStrength = valueComponents[2];

            //Build composite key
            StringBuilder keyBuilder = new StringBuilder();

            keyBuilder.append(keySite);
            keyBuilder.append(compositeKeyDelimiter);
            keyBuilder.append(relatedSite);
            keyBuilder.append(compositeKeyDelimiter);
            keyBuilder.append(linkStrength);

            combinedKey.set(keyBuilder.toString());

            relatedSiteAndLinkStrength.relatedSite = relatedSite;
            relatedSiteAndLinkStrength.linkStrength = linkStrength;

            context.write(combinedKey, relatedSiteAndLinkStrength);
        }
    }

    //Compares by natural key and then by link strength to sort things for the reducer
    public static class NaturalKeyAndLinkStrengthComperator extends WritableComparator {

        protected NaturalKeyAndLinkStrengthComperator() {
            super(Text.class, true);
        }

        @Override
        public int compare(WritableComparable w1, WritableComparable w2) {
            //Key to compare is "{key site}{delimiter}{related site}{delimiter}{link strength}"
            Text text1 = (Text)w1;
            Text text2 = (Text)w2;

            String[] text1components = text1.toString().split(compositeKeyDelimiter);
            String[] text2components = text2.toString().split(compositeKeyDelimiter);

            String text1KeySite = text1components[0];
            String text2KeySite = text2components[0];

            int keySiteComparison = text1KeySite.compareTo(text2KeySite);

            //Different key sites. no need for secondery sort
            if (keySiteComparison != 0) {
                return keySiteComparison;
            }

            int text1LinkStrength = Integer.parseInt(text1components[2]);
            int text2LinkStrength = Integer.parseInt(text2components[2]);

            //If the link strengths are the same, sort ascending by the related site
            if (text1LinkStrength == text2LinkStrength) {
                String text1RelatedSite = text1components[1];
                String text2RelatedSite = text2components[1];
                return text1RelatedSite.compareTo(text2RelatedSite);
            }

            return text1LinkStrength > text2LinkStrength ? -1 : 1;
        }
    }

    //Needed to make sure that all related sites of a key site will go to the same reduce invocation as values
    public static class NaturalKeyGroupingComperator extends WritableComparator {

        protected NaturalKeyGroupingComperator() {
            super(Text.class, true);
        }

        @Override
        public int compare(WritableComparable w1, WritableComparable w2) {
            //Key to compare is "{key site}{delimiter}{related site}{delimiter}{link strength}"
            Text text1 = (Text)w1;
            Text text2 = (Text)w2;

            String[] text1components = text1.toString().split(compositeKeyDelimiter);
            String[] text2components = text2.toString().split(compositeKeyDelimiter);

            String text1GroupingKey = text1components[0];
            String text2GroupingKey = text2components[0];

            int naturalKeyComparison = text1GroupingKey.compareTo(text2GroupingKey);

            return naturalKeyComparison;
        }
    }

    //Makes sure that all natural keys go to the same node
    public static class RelatedSitesNaturalKeyPartitioner extends Partitioner<Text, NullWritable> {
        @Override
        public int getPartition(Text key, NullWritable value, int numPartitions) {
            String[] keyComponents = key.toString().split(compositeKeyDelimiter);
            String partitionKey = keyComponents[0];
            int part = Math.floorMod(partitionKey.hashCode(), numPartitions); //Make sure modulo is positive number
            return part;
        }
    }

    public static class WebsiteStrengthFilterReducer extends Reducer<Text, RelatedSiteAndLinkStrength, Text, Text> {
        private Text keySiteText = new Text();
        private Text relatedSiteAndLinkStrengthText = new Text();

        private static int maxNumberOfSitesPerKeySite = 10;

        protected void reduce(Text key, Iterable<RelatedSiteAndLinkStrength> values, Context context) throws IOException, InterruptedException {
            String[] keyComponents = key.toString().split(compositeKeyDelimiter);

            String keySite = keyComponents[0];
            keySiteText.set(keySite);

            Iterator<RelatedSiteAndLinkStrength> it = values.iterator();
            for (int i = 0; i < maxNumberOfSitesPerKeySite; i++) {
                if (it.hasNext()) {
                    RelatedSiteAndLinkStrength relatedSiteAndLinkStrength = it.next();
                    relatedSiteAndLinkStrengthText.set(relatedSiteAndLinkStrength.relatedSite + " " + relatedSiteAndLinkStrength.linkStrength);
                    context.write(keySiteText, relatedSiteAndLinkStrengthText);
                } else {
                    return;
                }
            }
        }
    }

    public static Job createJob(Path input, Path output, Configuration configuration) throws Exception {
        Job job = Job.getInstance(configuration, "Website links to website matches");
        job.setJarByClass(TagMatcher.class);
        job.setMapperClass(WebsiteLinkStrengthToCompositeKeyMapper.class);
        job.setReducerClass(WebsiteStrengthFilterReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(RelatedSiteAndLinkStrength.class);

        job.setPartitionerClass(RelatedSitesNaturalKeyPartitioner.class);
        job.setSortComparatorClass(NaturalKeyAndLinkStrengthComperator.class);
        job.setGroupingComparatorClass(NaturalKeyGroupingComperator.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);
        return job;
    }
}
