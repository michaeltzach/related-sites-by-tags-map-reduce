import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.IOException;

public class TagAndSiteListToSiteLinkStrengthMapReduce {
    public static class WebsiteListToWebsiteLinkPointsMapper extends Mapper<Object, Text, Text, NullWritable> {
        private Text websiteURLPair = new Text();
        private static String websiteURLPairDelimiter = " ";

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            String[] websiteAndTagArray = value.toString().split("\\s+"); //split by whitespaces*

            //Starting from 1 because we have the tag at index 0
            for (int i = 1; i < websiteAndTagArray.length; i++) {
                String firstURL = websiteAndTagArray[i];

                for (int j = 1; j < websiteAndTagArray.length; j++) {
                    if (i == j) {
                        continue;
                    }

                    String secondURL = websiteAndTagArray[j];
                    websiteURLPair.set(firstURL + websiteURLPairDelimiter + secondURL);
                    context.write(websiteURLPair, NullWritable.get());
                }
            }
        }
    }

    public static class WebsiteLinksPointsReducer extends Reducer<Text, NullWritable, Text, IntWritable> {
        private IntWritable linkStrengthWritable = new IntWritable();

        protected void reduce(Text key, Iterable<NullWritable> values, Context context) throws IOException, InterruptedException {
            int linkStrenth = 0;

            for (NullWritable valueForLink : values) {
                linkStrenth ++;
            }

            linkStrengthWritable.set(linkStrenth);
            context.write(key, linkStrengthWritable);
        }
    }

    public static Job createJob(Path input, Path output, Configuration configuration) throws Exception {
        Job job = Job.getInstance(configuration, "Tags and site list to website link strength");
        job.setJarByClass(TagMatcher.class);
        job.setMapperClass(WebsiteListToWebsiteLinkPointsMapper.class);
        job.setReducerClass(WebsiteLinksPointsReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(NullWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);
        return job;
    }

}
