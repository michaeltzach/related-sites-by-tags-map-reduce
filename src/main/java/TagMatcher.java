import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;

import java.io.IOException;

public class TagMatcher {
    public static void main(String[] args) throws Exception {
        boolean verbose = true;

        Path input = new Path(args[0]);
        Path output = new Path(args[1]);
        Path tempPath = new Path("TempPath");
        Path tempPath2 = new Path("TempPath2");

        Configuration configuration = new Configuration();

        deletePaths(new Path[]{tempPath, tempPath2}, configuration);

        Job siteAndTagToTagAndSiteListJob = SiteAndTagToTagAndSiteListMapReduce.createJob(input, tempPath, configuration);
        boolean mapReduce1Success = siteAndTagToTagAndSiteListJob.waitForCompletion(verbose);
        if (mapReduce1Success == false) {
            System.exit(1);
        }

        Job tagAndSiteListToSiteLinkStrengthJob = TagAndSiteListToSiteLinkStrengthMapReduce.createJob(tempPath, tempPath2, configuration);
        boolean mapReduce2Success = tagAndSiteListToSiteLinkStrengthJob.waitForCompletion(verbose);
        if (mapReduce2Success == false) {
            System.exit(1);
        }

        Job siteLinksToTopRelatedSitesJob = SiteLinksToTopRelatedSitesMapReduce.createJob(tempPath2, output, configuration);
        boolean mapReduce3Success = siteLinksToTopRelatedSitesJob.waitForCompletion(verbose);
        if (mapReduce3Success == false) {
            System.exit(1);
        }

        deletePaths(new Path[]{tempPath, tempPath2}, configuration);

        System.exit(0);
    }

    private static void deletePaths(Path[] paths, Configuration configuration)  {
        try {
            FileSystem fs = FileSystem.get(configuration);
            for (Path path : paths) {
                fs.delete(path, true);
            }
        } catch (Exception e) {
            System.out.println("Error while deleting paths");
            System.out.println(e);
        }
    }
}

