# Version

* Hadoop version: 2.9.0
* Java 8

# How to build
* I added the jar to the repo at /out/artifacts/website_tag_matcher_jar
* The project has an artifacts file for Intelij Idea. You can clone the repo, open in Idea and then use "build -> Build Artifacts..." to rebuild.

# Tests
### How to run:
* Run the node scripts in the testFacory dir to create input and output files.
```
node test-factories/test1.js
node test-factories/test2.js
node test-factories/test3.js
```
* Run map reduce on input
* Compare the outputs
```
cmp true-output test-output
```

# Resources
* https://hadoop.apache.org/docs/current/hadoop-mapreduce-client/hadoop-mapreduce-client-core/MapReduceTutorial.html

* http://pkghosh.wordpress.com/2011/04/13/map-reduce-secondary-sort-does-it-all

* https://stackoverflow.com/questions/26693034/hadoop-strange-behaviour-reduce-function-doesnt-get-all-values-for-a-key?noredirect=1&lq=1

* https://developer.yahoo.com/hadoop/tutorial/module5.html#partitioning

* https://stackoverflow.com/questions/2633272/using-hadoop-are-my-reducers-guaranteed-to-get-all-the-records-with-the-same-ke?rq=1

* https://www.safaribooksonline.com/library/view/data-algorithms/9781491906170/ch01.html